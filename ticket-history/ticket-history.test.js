const axios = require('axios');
const fs = require('fs');
const { getTicketHistory, defaultRepositories, groupings } = require('./ticket-history');

jest.mock('axios');

describe('Test for ticket-history module', () => {
  const token = '123token';
  const groupResponse = {
    data: [
      {
        path_with_namespace: defaultRepositories[1],
      },
    ],
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should work', async (done) => {
    const response = {
      data: [
        {
          title: "Merge branch 'EPMSM-545-STABLE' into 'develop'",
          committed_date: '2020-08-07T17:18:03.000+00:00',
        },
        {
          title: '[EPMSM-545] feat (storybook) Perform detailed storybook setup (addons, sytnax, etc)',
          committed_date: '2020-08-07T17:18:02.000+00:00',
        },
        {
          title: "Merge remote-tracking branch 'EPMSM-545-STABLE' into 'develop'",
          committed_date: '2020-08-07T17:18:03.000+00:00',
        },
        {
          title:
            "Revert '[EPMSM-545] feat (storybook) Perform detailed storybook setup (addons, sytnax, etc)'",
          committed_date: '2020-08-07T17:18:03.000+00:00',
        },
        {
          title: 'hotfix',
          committed_date: '2020-08-07T17:18:03.000+00:00',
        },
      ],
      headers: {
        'x-total-pages': '1',
      },
    };
    const repositories = ['epm-s2m/virtual-room', 'epm-s2m/utils/storybook-addon-redux'];

    axios.get.mockResolvedValueOnce(response).mockResolvedValueOnce({
      data: [],
      headers: {
        'x-total-pages': '1',
      },
    });
    const dateSince = new Date();
    const dateUntil = new Date();
    const dateSinceISO = dateSince.toISOString();
    const dateUntilISO = dateUntil.toISOString();
    const expectedResult = {
      since: dateSince.toLocaleDateString(),
      until: dateUntil.toLocaleDateString(),
      'epm-s2m/virtual-room': {
        keys: ['EPMSM-545'],
        exceptional: ['hotfix'],
      },
    };
    await expect(getTicketHistory(token, dateSinceISO, dateUntilISO, true, repositories)).resolves.toEqual(
      expectedResult
    );
    const repositoryUrl = 'https://git.epam.com/api/v4/projects/epm-s2m%2Fvirtual-room/repository/commits';
    expect(axios.get).toHaveBeenCalledWith(repositoryUrl, {
      params: {
        since: dateSinceISO,
        until: dateUntilISO,
        per_page: 50,
        page: 1,
      },
      headers: { 'Private-Token': token },
    });
    done();
  });

  it('should work with defaults and handle errors', async (done) => {
    await expect(getTicketHistory()).rejects.toThrow();
    await expect(getTicketHistory(token)).rejects.toThrow();
    let dateSince = 'abc';
    await expect(getTicketHistory(token, dateSince)).rejects.toThrow();
    dateSince = '2020';
    const repositories = defaultRepositories;
    await expect(getTicketHistory(token, dateSince, dateSince, false, repositories, '123')).rejects.toThrow();

    const repositoryResponse = {
      data: [
        {
          title: '[EPMSM-545] feat (storybook) Perform detailed storybook setup (addons, sytnax, etc)',
          committed_date: '2020-08-07T17:18:02.000+00:00',
        },
        {
          title: 'hotfix',
          committed_date: '2020-08-07T17:18:03.000+00:00',
        },
      ],
      headers: {
        'x-total-pages': '2',
      },
    };
    axios.get.mockResolvedValueOnce(groupResponse).mockResolvedValue(repositoryResponse);

    const result = {
      since: new Date(dateSince).toLocaleDateString(),
      until: new Date().toLocaleDateString(),
    };
    // eslint-disable-next-line no-restricted-syntax
    for (const repository of repositories) {
      if (repository.split('/').pop() !== '*') {
        result[repository] = {
          keys: ['EPMSM-545'],
        };
      }
    }
    await expect(getTicketHistory(token, dateSince)).resolves.toEqual(result);

    done();
  });

  it('should work with groupBy = none', async (done) => {
    const response = {
      data: [
        {
          title: '[EPMSM-545] feat (storybook) Perform detailed storybook setup (addons, sytnax, etc)',
          committed_date: '2020-08-07T17:18:02.000+00:00',
        },
      ],
      headers: {
        'x-total-pages': '1',
      },
    };
    axios.get.mockResolvedValueOnce(groupResponse).mockResolvedValue(response);
    const result = {
      since: new Date('2020').toLocaleDateString(),
      until: new Date('2020').toLocaleDateString(),
      keys: ['EPMSM-545'],
    };
    await expect(
      getTicketHistory(token, '2020', '2020', true, defaultRepositories, groupings.none)
    ).resolves.toEqual({ ...result, exceptional: [] });
    axios.get.mockClear();
    axios.get.mockResolvedValueOnce(groupResponse).mockResolvedValue(response);
    await expect(
      getTicketHistory(token, '2020', '2020', false, defaultRepositories, groupings.none)
    ).resolves.toEqual(result);
    done();
  });

  it('should call fs.writeFile if savePath is provided', async (done) => {
    const mockWriteFile = jest.spyOn(fs.promises, 'writeFile').mockImplementation(() => {});
    axios.get.mockResolvedValueOnce(groupResponse);
    await getTicketHistory(
      token,
      '2020',
      '2020',
      false,
      defaultRepositories,
      groupings.none,
      './report.json'
    );
    expect(mockWriteFile).toHaveBeenCalledTimes(1);
    done();
  });

  it('should map tickets to commits', async (done) => {
    const response = {
      data: [
        {
          title: '[EPMSM-545] feat (storybook): Perform detailed storybook setup (addons, sytnax, etc)',
          committed_date: '2020-08-07T17:18:02.000+00:00',
          short_id: '123',
        },
        {
          title: '[EPMSM-859] feat (redux-mocking): commit a',
          committed_date: '2020-08-07T17:18:02.000+00:00',
          short_id: '124a',
        },
        {
          title: '[EPMSM-859] feat (redux-mocking): commit b',
          committed_date: '2020-08-07T17:18:10.000+00:00',
          short_id: '124b',
        },
      ],
      headers: {
        'x-total-pages': '1',
      },
    };
    const result = {
      since: new Date('2020').toLocaleDateString(),
      until: new Date('2020').toLocaleDateString(),
      keys: {
        'EPMSM-545': ['123'],
        'EPMSM-859': ['124a', '124b'],
      },
    };
    const repositories = ['epm-s2m/virtual-room'];
    const resultRepository = {
      since: result.since,
      until: result.until,
      'epm-s2m/virtual-room': {
        keys: result.keys,
      },
    };
    axios.get.mockResolvedValue(response);
    await expect(
      getTicketHistory(token, '2020', '2020', false, repositories, groupings.none, undefined, true)
    ).resolves.toEqual(result);
    await expect(
      getTicketHistory(token, '2020', '2020', false, repositories, groupings.repository, undefined, true)
    ).resolves.toEqual(resultRepository);
    done();
  });
});
