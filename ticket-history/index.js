const { Command } = require('commander');
const { getTicketHistory, groupings, defaultRepositories } = require('./ticket-history.js');

const program = new Command();

function commaSeparatedList(value) {
  return value.split(',');
}

program
  .version('0.0.2')
  .description(
    'Script for collecting changelog information from commit history to be used inside release preparation process'
  )
  .requiredOption('-t, --token <token>', 'Required. Gitlab private token')
  .requiredOption(
    '-s, --since <date1>',
    'Required. Only commits after or on this date will be returned. Format: any, but better use YYYY-MM-DDTHH:MM:SSZ'
  )
  .option(
    '-u, --until <date2>',
    'Only commits before or on this date will be returned. Format: any, but better use YYYY-MM-DDTHH:MM:SSZ. ' +
      '\nDefault: current date and time'
  )
  .option(
    '-e, --exceptional',
    'Produce report with exceptional message list which miss the ticket key for some reason'
  )
  .option('--map', 'Produce report with ticket key -> commit ids mapping.')
  .option('--save <path>', 'Specify path to save the report. Example: "./report.json"')
  .option(
    '-r, --repos <items>',
    `Comma separated list of repositories. Default: "${defaultRepositories}"`,
    commaSeparatedList
  )
  .option(
    '--group-by <target>',
    `Specifies the grouping (${Object.values(groupings)}). Default: ${groupings.repository}`
  );

program.parse(process.argv);

getTicketHistory(
  program.token,
  program.since,
  program.until,
  !!program.exceptional,
  program.repos,
  program.groupBy,
  program.save,
  !!program.map
).then(
  (result) => {
    // eslint-disable-next-line no-console
    console.log(result);
    process.exit(0);
  },
  (reason) => {
    // eslint-disable-next-line no-console
    console.error(reason);
    process.exit(1);
  }
);
