const axios = require('axios');
const _ = require('lodash');
const { promises: fs } = require('fs');
const defaultRepositories = require('./repositories.json');

const API_ENDPOINT = 'https://git.epam.com/api/v4';
const COMMIT_PATTERN = /^\[(\w+-\d+)]/i;

async function getGroupRepositories(token, groupId) {
  console.log(`> fetch repositories from ${groupId} group`);
  const url = `${API_ENDPOINT}/groups/${groupId.replace(/\//g, '%2F')}/projects`;
  const response = await axios.get(url, {
    params: {
      include_subgroups: true,
      per_page: 50,
    },
    headers: { 'Private-Token': token },
  });

  return _.map(response.data, (obj) => obj.path_with_namespace);
}

async function getRepositoryCommitHistory(token, repositoryId, dateSince, dateUntil, shouldAddExceptional) {
  console.log(`> fetch commits from ${repositoryId} repository`);
  const repositoryUrl = `${API_ENDPOINT}/projects/${repositoryId.replace(/\//g, '%2F')}/repository/commits`;
  const result = {
    keys: {},
  };
  if (shouldAddExceptional) {
    result.exceptional = [];
  }
  let page = 0;
  let totalPages = 0;
  do {
    // eslint-disable-next-line no-await-in-loop
    const response = await axios.get(repositoryUrl, {
      params: {
        since: dateSince.toISOString(),
        until: dateUntil.toISOString(),
        per_page: 50,
        page: ++page,
      },
      headers: { 'Private-Token': token },
    });
    totalPages = response.headers['x-total-pages'];
    _.map(response.data, (obj) => {
      const message = _.get(obj, 'title', '');
      const match = message.match(COMMIT_PATTERN);
      let key;
      if (match) {
        [, key] = match;
        if (!(key in result.keys)) {
          result.keys[key] = [];
        }
        result.keys[key].push(_.get(obj, 'short_id', ''));
      } else if (
        shouldAddExceptional &&
        message &&
        message.indexOf('Merge branch ') === -1 &&
        message.indexOf('Merge remote-tracking branch') === -1 &&
        message.indexOf('Revert') === -1
      ) {
        result.exceptional.push(message);
      }
      return key;
    });
  } while (page < totalPages);
  return result;
}

const groupings = {
  repository: 'repository',
  none: 'none',
};

async function saveResult(savePath, result) {
  if (!savePath) {
    return;
  }
  console.log(`> saving result to ${savePath}`);

  await fs.writeFile(savePath, JSON.stringify(result));
}

function arrayMerge(objValue, srcValue) {
  if (_.isArray(objValue)) {
    return objValue.concat(srcValue);
  }
  return srcValue;
}

async function getTicketHistory(
  token,
  dateSince,
  dateUntil,
  shouldAddExceptional = false,
  incomingRepositories = defaultRepositories,
  groupBy = groupings.repository,
  savePath = null,
  shouldMapToCommit = false
) {
  if (!(groupBy in groupings)) {
    throw Error(`groupBy must be in (${Object.keys(groupings)})`);
  }
  const since = new Date(dateSince);
  const until = dateUntil ? new Date(dateUntil) : new Date();
  if (!token || !since || !until)
    throw Error('Make sure you provide `token`, `dateSince` and `dateUntil` parameters in valid format.');
  const byRepoResult = {
    since: since.toLocaleDateString(),
    until: until.toLocaleDateString(),
  };
  const result = {
    ...byRepoResult,
    keys: {},
  };
  if (shouldAddExceptional) {
    result.exceptional = [];
  }
  let repositories = [];

  const handleIncomingRepos = async (repository) => {
    const repoParts = repository.split('/');
    if (repoParts.pop() === '*') {
      const group = repoParts.join('/');
      const groupRepositories = await getGroupRepositories(token, group);
      repositories.push(...groupRepositories);
    } else {
      repositories.push(repository);
    }
  };
  await Promise.all(incomingRepositories.map((r) => handleIncomingRepos(r)));

  repositories = _.uniq(repositories);

  const handleRepo = async (repository) => {
    byRepoResult[repository] = await getRepositoryCommitHistory(
      token,
      repository,
      since,
      until,
      shouldAddExceptional
    );
    if (groupBy === groupings.none) {
      result.keys = _.mergeWith(result.keys, byRepoResult[repository].keys, arrayMerge);
      if (shouldAddExceptional) {
        result.exceptional.push(...byRepoResult[repository].exceptional);
      }
    } else if (!shouldMapToCommit) {
      byRepoResult[repository].keys = Object.keys(byRepoResult[repository].keys);
      if (
        byRepoResult[repository].keys.length === 0 &&
        (!byRepoResult[repository].exceptional || byRepoResult[repository].exceptional.length === 0)
      ) {
        delete byRepoResult[repository];
      }
    }
  };

  await Promise.all(repositories.map((r) => handleRepo(r)));

  if (groupBy === groupings.none) {
    if (!shouldMapToCommit) {
      result.keys = Object.keys(result.keys);
    }
    await saveResult(savePath, result);

    return result;
  }

  await saveResult(savePath, byRepoResult);

  return byRepoResult;
}

module.exports = {
  getTicketHistory,
  groupings,
  defaultRepositories,
};
