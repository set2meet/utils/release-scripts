## Release scripts

### Ticket history

Script for collecting changelog information from commit history to be used inside release preparation process.

Use `-h` or `--help` flag to get list of all available commands:  
`npm run ticket-history -- -h`

Required params: `--token` and `--since`.  
'Token' is [Gitlab private token](https://git.epam.com/help/user/profile/personal_access_tokens.md) with following scopes: `api`, `read_api`, `read_repository`.

Example:  
command  
`npm run ticket-history -- -t 123 -s '2020/02/03 23:58'`  
will return all ticket keys from default repos (see ./ticket-history/repositories.json for default values)
since 2020/02/03 23:58 date.
