module.exports = {
  env: {
    commonjs: true,
    es6: true,
    es2020: true,
    node: true,
    jest: true,
  },
  extends: ['airbnb-base', 'plugin:prettier/recommended'],
  parserOptions: {
    ecmaVersion: 11,
  },
  plugins: ['prettier'],
  rules: {
    'eol-last': ['error', 'always'],
    'max-len': [
      'error',
      {
        code: 120,
      },
    ],
    'no-console': 0,
    'no-plusplus': 0,
    'global-require': 1,
    'prettier/prettier': 'error',
  },
};
