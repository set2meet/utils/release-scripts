module.exports = {
  testEnvironment: 'node',
  collectCoverage: true,
  coverageReporters: ['json-summary', 'lcov'],
  collectCoverageFrom: [
    //
    '**/*.{js,jsx}',
    '!**/*.config.*',
    '!**/node_modules/**',
    '!**/coverage/**',
    '!**/index.js',
    '!**/.eslintrc.js',
  ],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
};
